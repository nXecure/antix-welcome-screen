# antiX Welcome Screen

Relatively small yad script to introduce users to the antiX system.
It specifically introduces the antiX 19.2 full/base release, but should work on any version, including core.


It list many core apps, and contains buttons to launch them. If the app is not installed, it will ask the user and install it for them.

This information is easily available online, but I felt many users asking the same questions, and complicating themselves instead of using the system "the easy way".

Also, many users don't know the hidden gems that are included in antiX. My hope with this welcome screen is that they discover and learn about some of them.

# Content
At this moment, it contains 10 categories stored in Tabs

- **Welcome**
	- A simple greeter with launchers to **App Select** (search for installed apps), and both the gui and cli versions of the **antiX Control Centre** .
- **Live Tools**
	- Information about default live users and passwords.
	- Explanation about what the live system is, a description about some options, links to "Read more...", and a launcher for **Live USB maker tool**.
- **Pre-installation**
	- Information about default live users and passwords.
	- Advice about how to install, what to do before install, and launcher for the **antiX installer**.
	- Very small description about Frugal install.
- **Install/Launch Apps**
	- Explains what tools to use installing programs and packages, with launchers for **Synaptic**, **antiX Package Installer** and **cli-aptiX**.
	- Link to PPC's excellent guide to finding and installing programs, and hinting at other sources for programs not in the repo.
- **Updates**
	- How to update, keep your system updated, how to change the Repo mirror used, and small explanation about kernels.
	- Launchers for **Apt Notifier**, **antiX Updater** and the **Repo Manager**.
- **Desktops**
	- Explanation about the different sessions in antiX and how to easily change between them. Launcher for **Other Desktops**.
	- Includes launcher to fix the Other Desktops list.
	- The 4 Window Managers in antiX (icewm, jwm, fluxbox and herbstluftwm). Links to FAQ and about.
	- The 3 main Fil managers in antiX (ROX, SpaceFM and Midnight Commander). Launchers for each.
	- A little about customizing the desktop and installing Desktop Environments.
- **Startup/Default Apps**
	- How to autostart applications and commands in antiX. Launcher for **Add start**.
	- How to set up **Preferred Applications**, with its launcher.
	- About Alternatives Configurator
	- About changing default applications in File managers.
- **External Drives**
	- About automounting in antiX. Launcher to **Configure Automount**.
	- About manual mounting. Launcher to **antiX Mountbox** and to **Unplug Drive**.
	- About mounting Android and iOS devices, and their respective launchers.
- **Network**
	- Brief explanation about the main network managers in antiX 19, with some instructions for wifi. Launchers for **Connman** and **ceni**.
	- Warning about incompatibility between connman and ceni. Button to remove connman (and fix /etc/resolv.conf).
	- Button to unlock wifi softblock.
	- Launch editor to change /etc/networks/interfaces if needed.
- **Help**
	- Where to find FAQ and help.

# Screenshot

![](screenshot.png)

# Dependencies:
This has not been tested, but I believe the minimum dependencies to get it to work are:

*yad*,
*gksu* (included in antiX repo)
