#!/bin/bash
# -*- mode:sh -*-

# antiX welcome screen version 0.1
#Based on PPC's idea, trying to emulate the welcome screen in other distros.
#A lot of users don't know what antiX has to give. I hope this will help them use antiX to the fullest.
#
#Required packages: yad gksu

KEY=54321

#check if using desktop-defaults (antiX only)
if [ -x /usr/local/bin/desktop-defaults-run ]; then
	terminalexec="desktop-defaults-run -t"
	browserexec="desktop-defaults-run -b"
	editorexec="desktop-defaults-run -te"
	echo "desktop-session-antix installed"
else
	terminalexec="x-terminal-emulator -e"
	browserexec="xdg-open"
	editorexec="xdg-open"
	echo "no desktop-defaults-run"
fi

#check to see if the antiX docs are installed
if [ -d /usr/share/antiX/FAQ ]; then
	faqbrowser="/usr/share/antiX"
	echo "faqs-docs-antix installed"
else
	faqbrowser="https://download.tuxfamily.org/antix/docs-antiX-19"
	echo "accessing FAQ from the web"
fi

#check if connman installed and what version.
check_connman(){
	if [ -x /usr/bin/cmst ]; then
		connmanlaunch="cmst"
		connmanpath="/usr/bin/cmst"
		connmantext="antiX full comes with <b>cmst</b> as the default connman graphical interface. \
Once launched, switching wifi ON is very easy, and connecting to your wireless access point \
is done in the <b>Wireless</b> tab. You simply select the Access point, hit connect, \
enter the password and apply. If after rebooting it doesn't auto-launch, add <b>cmst -m</b> command to startup."
	elif [ -x /usr/bin/connman-ui-gtk ]; then
		connmanlaunch="connman-ui-gtk"
		connmanpath="usr/bin/connman-ui-gtk"
		connmantext="antiX base comes with <b>connman-ui-gtk</b> as \
the default connman graphical interface. Once launched, it will create an icon \
in the system tray (in your taskbar). Right clicking it will give you a menu to switch Wifi ON. \
After switching it on, left click the same icon (you may have to hold) and select your desired \
wireless access point. Enter the password and now you should be able to connect to the internet \
through wifi. If after rebooting it doesn't auto-launch, add <b>connman-ui-gtk</b> command to startup."
	else
		connmanlaunch="cmst"
		connmanpath="/usr/bin/cmst"
		connmantext=" If you don't want connman, please skip to a different topic.\\nThe best \
connman interface is <b>cmst</b>. Once launched, switching wifi ON is very easy, \
and connecting to your wireless access point is done in the <b>Wireless</b> tab. \
You simply select the Access point, hit connect, enter the password and apply. \
Right now, <b>Connman is not installed</b>. If you are ever interested in installing it, hit the button below."
	fi
}

#remove connman and recreate /etc/resolv.conf symlink
remove_connman(){
	#Check if connman is installed.
	if [ -x /usr/bin/cmst ]; then
		local connmanprog="cmst"
		local yadtext="Connman and cmst will be removed.\\nDo you want to continue?"
	elif [ -x /usr/bin/connman-ui-gtk ]; then
		local connmanprog="connman-ui"
		local yadtext="Connman and connman-ui-gtk will be removed.\\nDo you want to continue?"
	elif [ -x /usr/sbin/connmand ]; then
		local connmanprog=""
		local yadtext="Connman will be removed.\\nDo you want to continue?"
	else
		local connmanprog="not installed"
		local yadtext="Connman is not installed."
	fi
	echo "Connman condition: $connmanprog"
	
	#If not installed, inform the user
	if [[ $connmanprog == "not installed" ]]; then
		yad --image="gtk-dialog-error" --borders=10 --center \
		--title="Connman not in the system" --class="antiX-Welcome-screen" --name="install-check" \
		--form --align=center --separator="" --field="$yadtext":LBL '' \
		--button=gtk-quit:1
	elif ! [ -x /sbin/ceni ]; then 
		#If ceni is not installed, warn user that connman cannot be removed
		yad --image="gtk-dialog-error" --borders=10 --center \
		--title="CENI not in the system" --class="antiX-Welcome-screen" --name="install-check" \
		--form --align=center --separator="" \
		--field="CENI is not installed.\\nRemoving connman will lead to no network access.":LBL '' \
		--button=gtk-quit:1
	else
		#Ask if app should be removed
		yad --image="gtk-dialog-info" --borders=10 --center \
		--title="Remove Connman" --class="antiX-Welcome-screen" --name="install-check" \
		--form --align=center --separator="" \
		--field="$yadtext":LBL '' \
		--button=gtk-yes:0 --button=gtk-quit:1
		local exitcode=$?
		if [ $exitcode -eq 0 ]; then
			#check if DPKG database is locked
			if sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; then
				#tell the user that apt is locked by another program
				yad --image="gtk-dialog-error" --borders=10 --center \
				--title=$"APT is locked" --class="antiX-Welcome-screen" --name="apt-error" \
				--text=$"\\n APT is locked by another program." \
				--button=gtk-quit:1
			else
				#remove softblock
				x-terminal-emulator -e /bin/bash -c "sudo rfkill unblock all"
				#process removal
				x-terminal-emulator -T "Removing Connman" -e /bin/bash -c \
				"sudo apt-get -y remove --purge connman $connmanprog && sleep 0.1 &&
				sudo rm /etc/resolv.conf && sudo ln -s /etc/resolvconf/run/resolv.conf /etc/resolv.conf &&
				yad --center --width=250 --text-align=center --text='\\n Finished  ' \
				--button='gtk-ok':1 --title='Connman Removed'"
				#reconfigure /etc/resolv.conf
				#sudo rm /etc/resolv.conf
				#sudo ln -s /etc/resolvconf/run/resolv.conf /etc/resolv.conf
				#kill all connman intances -> not working. Needs to be rethought
				#old connman > /run/connman/resolv.conf
				#new ceni > /etc/resolvconf/run/resolv.conf
			fi
		fi
	fi
	
}

# check if installed and launch
check_launch(){
	#get app info
	local appname="$1"
	local apppath="$2"
	local applaunch="$3"
	local packagename="$4"
	local reinstalloption="$5"
	echo "Name: $appname, path: $apppath, Exec: $applaunch, package: $packagename, $reinstalloption"
	
	#check if app is installed
	if ! [ -x "$apppath" ]; then
		#check if reinstall is requested and change text accordingly
		if [[ $reinstalloption == "reinstall" ]]; then
			local yadtittle=$"Reinstalling $appname"
			local yadtext1=$"<b>$appname</b> is going to be fixed."
			local yadtext2=$"Do you want to reinstall it?"
			local installcommand="install --reinstall $packagename"
			echo "reinstall selected"
		else
			local yadtittle=$"$appname not installed"
			local yadtext1=$"<b>$appname</b> is not installed."
			local yadtext2=$"Do you want to install it?"
			local installcommand="install $packagename"
			echo "normal Install"
		fi
		#Ask if app should be installed
		yad --image="gtk-dialog-info" --borders=10 --center \
		--title="$yadtittle" --class="antiX-Welcome-screen" --name="install-check" \
		--form --align=center --separator="" \
		--field="$yadtext1":LBL '' \
		--field="$yadtext2":LBL '' \
		--button=gtk-yes:0 --button=gtk-quit:1
		local exitcode=$?
		if [ $exitcode -eq 0 ]; then
			#check if DPKG database is locked
			if sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; then
				#tell the user that apt is locked by another program
				yad --image="gtk-dialog-error" --borders=10 --center \
				--title=$"APT is locked" --class="antiX-Welcome-screen" --name="apt-error" \
				--text=$"\\n APT is locked by another program." \
				--button=gtk-quit:1
			else
				#process installation
				x-terminal-emulator -T "install $appname" -e /bin/bash -c \
				"sudo apt $installcommand && sleep 0.1 &&
				yad --center --width=250 --text-align=center --text='\\n Finished  ' \
				--button='gtk-ok':1 --title='apt - Installer'"
			fi
		fi
	else		
		#launch app
		($applaunch &)
	fi
}

#execute and prepare functions
export -f check_launch
export -f check_connman
export -f remove_connman
#executing functions
check_connman
#export variables
export terminalexec=$terminalexec
export editorexec=$editorexec
export connmanlaunch=$connmanlaunch
export connmanpath=$connmanpath

# Welcome tab
yad --plug=$KEY --tabnum=1 --borders=20 --form --scroll \
	--field="<b>antiX</b> is a fast, lightweight and easy to install systemd-free \
linux live CD distribution based on Debian Stable for Intel-AMD x86 \
compatible systems. antiX offers users the “antiX Magic” in an \
environment suitable for old and new computers. So don’t throw away \
that old computer yet!":LBL '' \
	--field="\\n<b>antiX 19.2</b> is based on Debian Buster and is built \
to be mean and lean. It usually comes by default with a <b>4.9 kernel</b>, \
as it will work better with older computers, but the user can update \
to a 4.19 or 5.x kernel if needed.":LBL '' \
	--field="\\nTo configure most things in your system, launch the \
antiX <b>Control Centre</b>.\\nTo easily search for installed apps, use \
<b>App Select</b>.\\n":LBL '' \
	--field="Control Centre":BTN 'bash -c "check_launch \"antiX Control Center\" \"/usr/local/bin/antixcc.sh\" \"antixcc.sh\" \"control-centre-antix\""' \
	--field="Control Centre (CLI version)":BTN "$terminalexec sudo antiX-cli-cc --pause &" \
	--field="App Select":BTN 'bash -c "check_launch \"App Select\" \"/usr/local/bin/app-select\" \"app-select\" \"app-select-antix\""' &

# Live Tools tab
yad --plug=$KEY --tabnum=2 --borders=20  --form --scroll \
	--field="For the Live system, the default passwords on live media are:":LBL '' \
	--field="    <b>User: demo</b>, password: demo":LBL '' \
	--field="    <b>Admin: root</b>, password: root":LBL '' \
	--field="":LBL '' \
	--field="Running live can be done from a CD/DVD, USB flash drive or External drive. \
Loading the whole system to RAM requires the boot parameter 'toram'.":LBL '' \
	--field="Saving system changes in a live system (as if installed) requires that \
your live-USB was created in a special way, for example using the <b>live USB Maker</b> tool.":LBL '' \
	--field="Live USB Maker tool":BTN 'bash -c "check_launch \"Live USB Maker\" \"/usr/bin/live-usb-maker-gui-antix\" \"su-to-root -X -c live-usb-maker-gui-antix\" \"live-usb-maker-gui-antix\""' \
	--field="To save all your changes on a live system, you can use <b>persistence</b> \
(save all changes on a separate file), or <b>remaster</b> all your changes \
(storing your whole system in the same file).":LBL '' \
	--field="Persistence option must be selected in the boot menu [<b>F5: Persist</b> menu \
in <b>Legacy</b> boot, or in <b>Customize Boot (text menus)</b> in <b>UEFI</b> systems]. \
There are many persistence options, but the two main options are:":LBL '' \
	--field="    <b>persist_root</b>: Dynamic root persistence saves file system changes in RAM, \
which can later be saved to the usb.":LBL '' \
	--field="    <b>p_static_root</b>: Static root persistence saves file system changes \
continuously, writing directly on the USB device.":LBL '' \
	--field="":LBL '' \
	--field="The most relevant live Tools can be found in the <b>Live</b> tab inside the antiX Control Centre.":LBL '' \
	--field="Control Centre":BTN 'bash -c "check_launch \"antiX Control Center\" \"/usr/local/bin/antixcc.sh\" \"antixcc.sh\" \"control-centre-antix\""' \
	--field="\\nYou can learn more about how the live system works, or about persistence \
and remaster in the antiX FAQ.":LBL '' \
	--field="About Live Systems in antiX":BTN "$browserexec https://antixlinux.com/the-most-extensive-live-usb-on-the-planet/" \
	--field="About Persistence (FAQ)":BTN "$browserexec $faqbrowser/FAQ/persistence.html" \
	--field="About Remaster (FAQ)":BTN "$browserexec $faqbrowser/FAQ/remaster.html" &

# Pre-installation tab
yad --plug=$KEY --tabnum=3 --borders=20 --form --scroll \
	--field="For the Live system, the default passwords on live media are:":LBL '' \
	--field="    <b>User: demo</b>, password: demo":LBL '' \
	--field="    <b>Admin: root</b>, password: root":LBL '' \
	--field="":LBL '' \
	--field="           <b>Installation to Disk</b>":LBL '' \
	--field="All updates and new software added pre-installation \
will be included when you are ready to install antiX to your system.":LBL '' \
	--field="You can auto-install using the entire disk or proceed with a Custom install (can also preserve /home). Make sure to select your correct keyboard layout, language (locale) and timezone. If you have changed how your system looks and you want to preserve it, select <b>Save live desktop changes</b>.":LBL '' \
	--field="Install antiX":BTN 'bash -c "check_launch \"antiX Installer\" \"/usr/sbin/minstall\" \"su-to-root -X -c minstall\" \"antix-installer\""' \
	--field="<b>Custom Install</b> requires you to have already created the necessary partitions. The only fundamental partition you need is the <b>root</b> partition, that will save ALL programs and configurations of your antiX system (minimum required is 5 GB). Then set all options (except swap) to point to the root partition. If you want a separate partition to store your user files and configuration, you will need a <b>home</b> partition (can be usable by other linux systems). If you are very low on RAM (less than 2 GB) or want to be able to hibernate your system (not the same as suspend), you need a swap partition with at least the size of your RAM. If you want your antiX boot in a different partition, you need a <b>boot</b> partition (for advanced users). If you already have a home partition and want to preserve it, don't forget to select the option <b>Preserve data in /home</b>.":LBL '' \
	--field="<b>GRUB installation</b> may be tricky if you do not know what you are doing. If you are on a new system, or are using UEFI booting, you need to install grub in ESP (EFI System Partition). For old systems or that are using Legacy BIOS, you will only need to install grub if there is no other boot manager installed. On these systems, installing GRUB in MBR will override the system's boot manager. If installed on PBR it will only install in the (root) partition containing your antiX system.":LBL '' \
	--field="":LBL '' \
	--field="           <b>Frugal Install</b>":LBL '' \
	--field="There is also a different option for installing antiX without \
occupying/rewriting partitions on your system. We are talking about a Frugal installation. \
It is equivalent to a live persistent system but running directly on your hard-drive \
(without any partition formatting needed).":LBL '' \
	--field="Its only limitation is having to boot it differently \
than an installed system. If you are interested, read information about \
it and reboot with frugal boot options.":LBL '' \
	--field="About Frugal (FAQ)":BTN "$browserexec $faqbrowser/FAQ/frugal.html" &

#Search and Install Apps
yad --plug=$KEY --tabnum=4 --borders=20  --form --scroll \
	--field="There is an enormous number of programs and packages (including <b>kernels</b>) available from the antiX and Debian repos.":LBL '' \
	--field="An easy way to find and install popular apps is to use the <b>antiX package installer</b>, which will also provide other Desktop Environments and Window Managers, the latest kernels, the Latest versions of LibreOffice and Gimp, and also some very demanded non-free applications.":LBL '' \
	--field="antiX Package Installer":BTN 'bash -c "check_launch \"antiX Package Installer\" \"/usr/bin/packageinstaller\" \"gksu packageinstaller\" \"packageinstaller\""' \
	--field="<b>Synaptic Package manager</b> is the most complete program to manage packages on antiX. You will be able to find updates, install and uninstall packages, information about each of them and all available kernels.":LBL '' \
	--field="Synaptic package manager":BTN 'bash -c "check_launch \"Synaptic package manager\" \"/usr/sbin/synaptic\" \"gksu synaptic\" \"synaptic\""' \
	--field="From any antiX edition, one can install programs using the command line program <b>cli-aptiX</b> (containing a list of very demanded cli and gui programs, and an easy way to find and install kernels).":LBL '' \
	--field="cli-aptiX":BTN "$terminalexec sudo cli-aptiX --pause &" \
	--field="You need to remember that you can only use one package manager/installer at a time.":LBL '' \
	--field="If you are having trouble installing applications, please make sure that that you update the package list first.":LBL '' \
	--field="Update package list":BTN "$terminalexec sudo apt update" \
	--field="":LBL '' \
	--field="All apps installed on the system can be launched from the Applications menu, \
using the <b>App Select</b> program, in the /usr/share/applications/ folder, etc.":LBL '' \
	--field="Search installed Apps - <b>App Select</b>":BTN 'bash -c "check_launch \"App Select\" \"/usr/local/bin/app-select\" \"app-select\" \"app-select-antix\""' \
	--field="If you install a program by installing a .deb file, compiling from source \
or in other manual ways, the menu will not show them until you <b>Refresh Menu</b>, \
found in the main menu for icewm, fluxbox or jwm.":LBL '' \
	--field="If you are looking for more sources to install programs in antiX, please read PPC's guide about installing programs in antiX.":LBL '' \
	--field="PPC - How to install applications":BTN "$browserexec https://www.antixforum.com/forums/topic/how-to-install-applications-2020-version/" &

#Update tab
yad --plug=$KEY --tabnum=5 --borders=20  --form --scroll \
	--field="If you want to automatically get notified whenever new updates appear, run apt notifier and add it to startup so that it will always be in your system tray.":LBL '' \
	--field="You can also manually check for updates using the program <b>antiX Updater</b>.":LBL '' \
	--field="Apt Notifier":BTN 'bash -c "check_launch \"Apt Notifier\" \"/usr/bin/apt-notifier\" \"apt-notifier\" \"apt-notifier\""' \
	--field="antiX Updater":BTN 'bash -c "check_launch \"antiX updater\" \"/usr/local/bin/yad-updater\" \"yad-updater\" \"antix-goodies\""' \
	--field="When a new point release is announced (for example, you have 19.1 installed and 19.2 is released), your system will automatically get the corresponding updates so you don't have to install from zero, but your system will still reflect (in name) the same point release it was installed with. This is to ease troubleshooting in the future.":LBL '' \
	--field="If you want to get your updates with the best download speed, or manually select a specific mirror, you can change the repo mirror using the <b>Repo Manager</b>.":LBL '' \
	--field="Repo Manager":BTN 'bash -c "check_launch \"antiX Repo Manager\" \"/usr/bin/repo-manager\" \"su-to-root -X -c repo-manager\" \"repo-manager\""' \
	--field="If you want to check what new packages or updates were recently installed (or removed) you can check in /var/log/apt/history.log for all apt related activity.":LBL '' \
	--field="           <b>Updating the kernel":LBL '' \
	--field="To avoid breaking your system with an update, <b>antiX kernels don't automatically update</b>. If you want or need a new kernel, you need to install it manually with any of the methods described in the Install Apps tab. antiX has version 4.9, 4.19 and 5.x kernels available from the repos.":LBL '' &
	
#Desktops tab
yad --plug=$KEY --tabnum=6 --borders=20  --form --scroll \
	--field="antiX (base and full) comes with 3 different desktop configurations. From more features (and more RAM use) to less are:":LBL '' \
	--field="    <b>desktop + window-manager</b>: a file manager (ROX or SpaceFM) displays icons/launchers on the desktop, and manages the wallpaper, automounting and files. The window manager (IceWM, JWM or fluxbox) will control the rest. Example: <b>Rox-IceWM</b>":LBL '' \
	--field="    <b>window-manager</b>: There are no icons on the desktop. The window manager is in charge of everything (from wallpaper to the rest of the desktop's behavior). The default file manager can be selected manually.  Example: <b>Fluxbox</b>":LBL '' \
	--field="    <b>minimal window-manager</b>: The most minimal of all sessions. No icons in the desktop, no autostarting of programs, less resource usage. Ideal for systems very limited on RAM. Example: <b>Minimal-JWM</b>":LBL '' \
	--field="In this way, all sessions will have one of the previous configurations. You can change session at any time from the login screen (F1), from the Other Desktops menu entry or using the Other Desktops application.":LBL '' \
	--field="Other Desktops":BTN 'bash -c "check_launch \"Desktop Session antiX\" \"/usr/local/lib/desktop-session/desktop-session-menu-window\" \"/usr/local/lib/desktop-session/desktop-session-menu-window ignore-settings\" \"desktop-session-antix\""' \
	--field="If at some point you see that you cannot find the session you were previously using between the other desktop options, you can click the next button to fix and reinstall the list, so all sessions are again recognized.":LBL '' \
	--field="Fix antiX Session list":BTN 'bash -c "check_launch \"Desktop session list\" \"no\" \"no\" \"desktop-session-antix\" \"reinstall\""' \
	--field="":LBL '' \
	--field="           <b>Window Managers</b>":LBL '' \
	--field="antiX includes four window managers by default.":LBL '' \
	--field="    <b>Icewm</b> brings an experience very similar to modern desktops environments with a fraction of the resource use. Very customizable.":LBL '' \
	--field="    <b>fluxbox</b> tries to reduce clutter in the desktop, with no launchers in the taskbar and simple right-clicking menus.":LBL '' \
	--field="    <b>JWM</b> also gives a similar experience to a desktop environment, operating even faster than the previous window managers and using even less resources.":LBL '' \
	--field="    <b>herbstluftwm</b> is an extremely light manual tiling window manager that is configured with a bash script (launched on startup) and that uses tags for layouts.":LBL '' \
	--field="You can learn more about them in the FAQ and official page.":LBL '' \
	--field="jwm - antiX FAQ":BTN "$browserexec $faqbrowser/FAQ/jwm.html" \
	--field="Icewm - antiX FAQ":BTN "$browserexec $faqbrowser/FAQ/icewm.html" \
	--field="fluxbox - antiX FAQ":BTN "$browserexec $faqbrowser/FAQ/fluxbox.html" \
	--field="About herbstluftwm":BTN "$browserexec https://herbstluftwm.org" \
	--field="":LBL '' \
	--field="           <b>File managers</b>":LBL '' \
	--field="antiX base and full ship with three file managers.":LBL '' \
	--field="    <b>ROX</b> is a very simple and lightweight file manager that provides easy to understand interface. Many buttons can be configured and it is usually used with a few open instances to copy easily from one window to another.":LBL '' \
	--field="    <b>SpaceFM</b> is a complete file manager, like the ones found in full Desktop Environments. It can display available drives to easily mount, can use tabs for separate folders and permits up to 4 side-by-side panels.":LBL '' \
	--field="    <b>Midnight Commander (MC)</b>. MC is a visual file manager that runs in terminal. It can work with mouse input and allows you to copy, move, delete and search files and folders, and even run commands in the subshell.":LBL '' \
	--field="ROX Filer":BTN 'bash -c "check_launch \"ROX Filer\" \"/usr/bin/rox-filer\" \"rox-filer $HOME\" \"rox-filer desktop-defaults-rox-antix\""' \
	--field="SpaceFM":BTN 'bash -c "check_launch \"SpaceFM\" \"/usr/bin/spacefm\" \"spacefm $HOME\" \"spacefm desktop-defaults-spacefm-antix\""' \
	--field="Midnight Commander":BTN 'bash -c "check_launch \"Midnight Commander\" \"/usr/bin/mc\" \"$terminalexec mc / $HOME\" \"mc\""' \
	--field="If you dislike the <b>one-click open</b> in SpaceFM and ROX (and want the two clicks to open behavior), change it in their respective options inside the file manager.":LBL '' \
	--field="For <b>ROX</b>, right click an empty space, select <b>Options...</b> and disable Single-click navigation.":LBL '' \
	--field="For <b>SpaceFM</b>, on the top menu, go to View, select <b>Preferences</b> and disable One click opens file.":LBL '' \
	--field="":LBL '' \
	--field="           <b>Customizing the desktop</b>":LBL '' \
	--field="Each Window Manager has it's own themes and customization specific to them. Other things that can be customized are:":LBL '' \
	--field="<b>Arandr</b> takes care of all display related things (screen orientation, resolution, position, multi-screen, etc). <b>LX Appearance</b> and <b>QT5 Settings</b> control how GTK and QT programs look like (respectively) and are also used to set the icon theme. If you need to change the desktop's background, use antiX' <b>Wallpaper</b> app. More options available in the Control Centre.":LBL '' \
	--field="Control Centre":BTN 'bash -c "check_launch \"antiX Control Center\" \"/usr/local/bin/antixcc.sh\" \"antixcc.sh\" \"control-centre-antix\""' \
	--field="":LBL '' \
	--field="           <b>Installing other Desktop Environments</b>":LBL '' \
	--field="If you are interested in using other Window Managers or Desktop Environments, you can easily install them from the antiX <b>Package Installer</b>. After installing, log out from you system and select the Desktop Environment you want to use (F1 if in default display Manager Slim to scroll between all possible sessions).":LBL '' &
	
#Startup/Defaults tab
yad --plug=$KEY --tabnum=7 --borders=20  --form --scroll \
	--field="           <b>Autostarting programs</b>":LBL '' \
	--field="By default in antiX, you need to manually tell the system what programs/scripts you want to autostart (they will not set themselves) when booting. You can use the <b>Add Start</b> program, or manually edit the startup file for your window manager (located in your home folder).":LBL '' \
	--field="Add program to startup - <b>Add Start</b>":BTN 'bash -c "check_launch \"Add Start antiX\" \"/usr/local/bin/add-start\" \"add-start\" \"add-start-antix\""' \
	--field="If you want the change to take effect for all different sessions (desktop configurations), you will need to add the autostart command to ~/.desktop-session/startup file instead of using the startup file of your current window manager.":LBL '' \
	--field="":LBL '' \
	--field="           <b>antiX Preferred Applications</b>":LBL '' \
	--field="antiX specific apps and menus have an easy way to call and set some default applications. These default applications can be changed using <b>Preferred Applications</b>. You will be able to set the default Terminal, Web browser, File manager, Email client, Text editor, Image viewer, Video player and Audio player. If you cannot find the application you want to set, search in the /usr/share/applications folder insider Preferred Applications.":LBL '' \
	--field="Preferred Applications":BTN 'bash -c "check_launch \"Desktop Session antiX\" \"/usr/local/bin/desktop-defaults-set\" \"desktop-defaults-set\" \"desktop-session-antix\""' \
	--field="":LBL '' \
	--field="           <b>Other Default Applications</b>":LBL '' \
	--field="You can also set other system default applications using the <b>Alternatives Configurator</b>. Using this program will take some learning, recommended for advanced users.":LBL '' \
	--field="Alternatives Configurator":BTN 'bash -c "check_launch \"Alternatives Configurator\" \"/usr/bin/galternatives\" \"galternatives\" \"galternatives\""' \
	--field="When using a file manager, if you want a specific file type (mimetype) to launch with a different program (mimeapp) than the one it now uses, you will have to configure it on said file manager.":LBL '' \
	--field="For <b>SpaceFM</b>, right click the file, navigate the Open submenu and click on <b>Choose...</b>. Choose the app you want to use in All Apps tab, and make sure you enable Set as default application for this file type.":LBL '' \
	--field="For <b>ROX</b>, right click the file and click on <b>Set Run Action...</b>. You can either enter the shell command for the program or drop the .desktop file for that application (found in /usr/share/applications).":LBL '' &
	
#External Drives tab
yad --plug=$KEY --tabnum=8 --borders=20  --form --scroll \
	--field="           <b>Automounting</b>":LBL '' \
	--field="By default, antiX is set to automount all CDs and external drives that you connect to your computer. This behavior can be disabled and enabled at any time with the <b>Automount Configuration</b> tool.":LBL '' \
	--field="Configure Automount":BTN 'bash -c "check_launch \"Automount Configuration\" \"/usr/local/bin/automount-config\" \"automount-config\" \"automount-antix desktop-session-antix\""' \
	--field="           <b>Manual Mounting</b>":LBL '' \
	--field="If you have trouble mounting drives and partitions (with SpaceFM for example), you can manually mount them. If you don't know how to do so from a terminal, you can also use <b>antiX Mountbox</b> to achieve this graphically.":LBL '' \
	--field="antiX Mountbox":BTN 'bash -c "check_launch \"antiX Mountbox\" \"/usr/local/bin/mountbox\" \"mountbox\" \"mountbox-antix\""' \
	--field="Unmounting devices can be easily achieved from the file manager or from the <b>Unplugdrive</b> script.":LBL '' \
	--field="Unplug Drive":BTN 'bash -c "check_launch \"antiX Unplugdrive\" \"/usr/local/bin/unplugdrive.sh\" \"unplugdrive.sh\" \"antix-goodies\""' \
	--field="":LBL '' \
	--field="           <b>Mounting mobile devices</b>":LBL '' \
	--field="    <b>Android devices (MTP)</b>: To connect android devices, first enable <i>File sharing</i> option on them, connect the device through a USB port, and launch <b>Android Device USB connect</b>.":LBL '' \
	--field="    <b>Apple devices</b>: Mounting Apple devices in linux is sometimes cumbersome. You can achieve this easily for some iOS devices using the <b>iDevice Mounter</b> app.":LBL '' \
	--field="Android Device USB Connect":BTN 'bash -c "check_launch \"Android Device USB Connect\" \"/usr/local/bin/android-device-usb-connect.sh\" \"android-device-usb-connect.sh\" \"antix-goodies\""' \
	--field="iDevice Mounter":BTN 'bash -c "check_launch \"iDevice Mounter\" \"/usr/bin/idevice-mounter\" \"idevice-mounter\" \"idevice-mounter iphone-antix\""' &
	
#Network tab
yad --plug=$KEY --tabnum=9 --borders=20  --form --scroll \
	--field="antiX comes with many network managers. The default one is <b>Connman</b>, a very complete one that even lets you set mobile telephony (GSM/UMTS) connections. <b>CENI</b> is also a very complete terminal network manager, but it lacks a taskbar icon to manage the connections. They both cannot work together, so we recommend you first try if Connman works for you.":LBL '' \
	--field="           <b>Setting up Wifi with Connman</b>":LBL '' \
	--field="$connmantext":LBL '' \
	--field="Connman":BTN 'bash -c "check_launch \"Connman - Network manager\" \"$connmanpath\" \"$connmanlaunch\" \"connman $connmanlaunch\""' \
	--field="More advanced options (like setting up a mobile connection, VPN and others) will need more researching. You can read more about Connman in its website or searching the interwebs.":LBL '' \
	--field="If you have tried using ceni before launching connman, and you found it impossible to search for Access points in Connman, you will have to remove all wifi entries in /etc/network/interfaces (below <i>iface lo inet loopback</i>) that were created by ceni, before you can get connman to work properly.":LBL '' \
	--field="":LBL '' \
	--field="           <b>Setting up Wifi with CENI</b>":LBL '' \
	--field="If you don't want connman or it doesn't work for you, there is still <b>ceni</b>. For it to work properly, you will first have to remove Connman if you haven't already.":LBL '' \
	--field="Remove Connman":BTN 'bash -c remove_connman' \
	--field="Ceni is a terminal based network manager that accepts mouse input. To set up any connection with Ceni, you need to launch it and select the correct interface. For a wireless connection, select the wireless interface (generally <b>wlan0</b>), and scan for access points. When you see your access point, select it and write your password. Click Accept twice and you should properly connect to the internet.":LBL '' \
	--field="CENI":BTN 'bash -c "check_launch \"CENI\" \"/sbin/ceni\" \"$terminalexec sudo ceni\" \"ceni\""' \
	--field="If the wireless interface cannot see any connection, you could try unlocking the wifi through software.":LBL '' \
	--field="Unlock Wifi (software unlock)":BTN "$terminalexec sudo rfkill unblock all" \
	--field="If you want to use a different network manager, remember you have to delete all network entries saved in  /etc/network/interfaces (below <i>iface lo inet loopback</i>)":LBL '' \
	--field="Edit /etc/network/interfaces":BTN "gksu xdg-open /etc/network/interfaces" &
	
#Help tab
yad --plug=$KEY --tabnum=10 --borders=20  --form --scroll \
	--field="Find more info about antiX in the <b>Frequently Asked Questions (FAQ)</b>":LBL '' \
	--field="You will find many video tutorial for antiX and MX on <b>runwiththedolphin</b> (Dolphin_Oracle)'s youtube channel":LBL '' \
	--field="If you enjoy <b>Facebook</b>, you can also join the antiX community there.":LBL '' \
	--field="antiX Linux FAQ":BTN "$browserexec $faqbrowser/FAQ/index.html" \
	--field="runwiththedolphin | Youtube":BTN "$browserexec https://www.youtube.com/channel/UCFWlej2CSKlXW5uE9opXukQ" \
	--field="antiX Linux Public Group | Facebook":BTN "$browserexec https://www.facebook.com/groups/89717832488/" \
	--field="Finally, if you are having a lot of trouble and google is not helping, consider asking for help in the <b>antiX forums</b>, providing the output of the command <i>inxi -Fxz</i> with your post.":LBL '' \
	--field="antiX Linux Official forums":BTN "$browserexec https://www.antixforum.com" \
	--field="antiX is delivered to you for free. Most things take time and effort to create and maintain, but the servers we use cost money. If you find you have some free change, please consider donating it to maintain the servers.":LBL '' \
	--field="Donate for server maintenance":BTN "$browserexec https://www.antixforum.com/forums/topic/fund-drive/" \
	--field="\\n<b>THANK YOU</b> for using antiX":LBL '' &


# run main dialog
yad --notebook --tab-pos="left" --key=$KEY --tab="Welcome" --tab="Live Tools" \
	--tab="Pre-installation" --tab="Install/Launch Apps" --tab="Updates" \
	--tab="Desktops" --tab="Startup/Default Apps" --tab="External Drives" --tab="Network" \
	--tab="Help" --window-icon=gnome-control-center --title="antiX Welcome screen" \
	--class="antiX-Welcome-screen" --name="antix-welcome" \
	--height=550 --width=700 --image="/usr/local/lib/antiX/antiX-logo.png" --image-on-top \
    --center --text="<b><big>Welcome to antiX 19.2 Hannie Schaft</big></b>" \
    --button='gtk-close':1
